package ccalgary.transit.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


public class MainActivity extends Activity {
    public String stops[][];
    public static final String TAG = "Main";
    public static double stopLat;
    public static double stopLon;
    public static NotificationManager notificationManager;
    private LocationManager locationManager;
    private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in Milliseconds
    public static String stop_name;
    public static Context sContext;
    public LocationListener MyLocationListener;
    public SharedPreferences settings;

    /**
     *
     *
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        sContext = getApplicationContext();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
        }else{
            showGPSDisabledAlertToUser();
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        MyLocationListener = new MyLocationListener();

        try {
            InputStream is = getResources().getAssets().open("stops.txt");
            if (is == null) {
                Log.d(TAG, "still can't find it boss");
            }
            if (is != null) {
            stops = convertStreamToString(is);
            }

        }catch (IOException e) {

            Log.d(TAG, "IOE exception" + e);
            e.printStackTrace();
        }
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);


    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//
//    }
    @Override
    protected void onPause() {
        super.onPause();
        boolean tracking = settings.getBoolean("Tracking", false);
        if (tracking == false){
            notificationManager.cancel(1);
            locationManager.removeUpdates(MyLocationListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void notification(View view) {
        startNotification();

    }

    private void startNotification() {
                    Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
            Notification notification = createNotification();
            notification.setLatestEventInfo(getApplicationContext(), "Proximity Alert!", "You are are close to your stop.", pendingIntent);
            notificationManager.notify(1000,notification);
            Toast.makeText(getApplicationContext(), "you are close to your stop!" , Toast.LENGTH_SHORT).show();
            Log.i("Main", "makeNotification has been triggered");
        }

    private static Notification createNotification() {
        Notification notification = new Notification();
        notification.icon = R.drawable.ic_launcher;
        notification.when = System.currentTimeMillis();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.ledARGB = Color.WHITE;
        notification.ledOnMS = 1500;
        notification.ledOffMS = 1500;
        return notification;

    }

    private void showNotification() {
        Notification notification = new Notification(R.drawable.ic_launcher, "Transit alarm is running", System.currentTimeMillis());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), Notification.FLAG_ONGOING_EVENT);
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notification.setLatestEventInfo(this, "Transit Alarm", "Transit Alarm will tell you when you get close to your stop",pendingIntent );
        notificationManager.notify(1,notification);
    }

    public static String[][] convertStreamToString(InputStream is)
            throws IOException {
        Writer writer = new StringWriter();

        char[] buffer = new char[2048];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            is.close();
        }
        String text = writer.toString();
 //       Log.d("Main", text);

        String[] lines = text.split("\n");
        Log.d("Main"," length of lines is " + lines.length  );
        String[][] stopData;
        stopData = new String[lines.length][50];

        if (lines != null) {


           for (int i =1; i < lines.length; i++ ) {
                String line = lines[i];

                String[] info = line.split(",");
                for (int j = 0; j < 9; j++){
                    stopData[i][j] = info[j];
           }
        }
        }


        return stopData;
    }


    public void Search(View view) {

        EditText editText = (EditText) findViewById(R.id.StopNumber);
        String stopNeeded = editText.getText().toString();
        Toast.makeText(getApplicationContext(), "You will now be notified of your stop!" , Toast.LENGTH_SHORT).show();

        for (int i=0; i < stops.length; i++) {
            if (stopNeeded.equals(stops[i][0])) {
                String stringLat = stops[i][4];
                String stringLon = stops[i][5];
                stop_name=stops[i][2];
                stopLat = Double.parseDouble(stringLon);
                stopLon = Double.parseDouble(stringLat);
                Log.d(TAG, "lat and lon are " + stringLat + " "+ stringLon);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("Tracking", true);
                editor.commit();
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATE, MINIMUM_DISTANCECHANGE_FOR_UPDATE, MyLocationListener);
//                Log.d(TAG, "stop found");
                break;
            }
        }
        showNotification();
        setContentView(R.layout.gps);
        TextView textView = (TextView)findViewById(R.id.stop_name);
        textView.setText(stop_name);


    }

    public void cancel(View view) {
        //need to remove this from a static context
        locationManager.removeUpdates(MyLocationListener);
        notificationManager.cancel(1);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("Tracking", false);
        editor.commit();
        Intent intent = new Intent(sContext,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }



    public class MyLocationListener extends Application implements LocationListener {
        public void onLocationChanged(Location location) {


            if ((stopLon != 0) && (stopLat != 0)) {

                Location location2 = new Location("stop");
                location2.setLatitude(stopLon);
                location2.setLongitude(stopLat);
                float distance = location2.distanceTo(location);
                if (distance <=100.0) {
                    Log.d(TAG, "distance triggered");
                    startNotification();
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putBoolean("Tracking", false);
                    editor.commit();
                    locationManager.removeUpdates(this);
                    notificationManager.cancel(1);
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                }

//                String string = "distance to stop is " + distance;
//                Toast.makeText(getAppContext(), string , Toast.LENGTH_SHORT).show();
            }

        }
        public void onStatusChanged(String s, int i, Bundle b) {
        }
        public void onProviderDisabled(String s) {
        }
        public void onProviderEnabled(String s) {
        }
    }

 }

